<?PHP
include "config.php";
$s = $_SERVER['QUERY_STRING'];

if ($s==null) {echo "
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<meta name=\"description\" content=\"anonymize your links with $sitename\" />
	<meta name=\"keywords\" content=\"anonymous,link,url,redirect,forum,board,script,domain,external,hompage\" />
	<title>$sitename | link to other sites anonymously</title>
	<link rel='shortcut icon' href='http://$url/assets/ico/favicon.ico'>

	<link rel=\"stylesheet\" type=\"text/css\" href=\"http://$url/assets/css/bootstrap.min.css\" />
	<link rel='stylesheet' type='text/css' href='http://$url/assets/css/font-awesome.min.css' />
	<link rel='stylesheet' type='text/css' href='http://$url/assets/css/style.css' />
	<link rel='stylesheet' type='text/css' href='http://$url/assets/css/tipsy.css' />
    <script src='http://$url/anonymizerTool.js'></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src='http://$url/assets/js/html5shiv.js'></script>
      <script src='http://$url/assets/js/respond.min.js'></script>
	  <link rel='stylesheet' type='text/css' href='http://$url/assets/css/font-awesome.ie7.min.css' />
    <![endif]-->
    <script> 
    jQuery.noConflict();
    jQuery(function() {
    jQuery('a').tipsy({gravity: jQuery.fn.tipsy.autoNS});
    jQuery('img').tipsy({gravity: jQuery.fn.tipsy.autoNS});
    });
    </script>
	<script src='http://$url/assets/js/bootstrap.min.js'></script>
	<script src='http://$url/assets/js/application.js'></script>
	<script src='http://$url/assets/js/holder.js'></script>
	<script src='http://$url/assets/js/jquery.tipsy.js'></script>
	<script src='http://$url/assets/js/jquery.js'></script>
</head>

	
<body>
		<div id='header'>
			<div class='container'>
				<p class='text-muted credit'><a title='$sitename' href='//$url/'><i class='tgfa tgfa-blue pull-left'></i></a><h1>anonymize your links with $sitename</h1>
				<a href='https://twitter.com/tgfa_team' class='twitter-follow-button' data-link-color='#2980b9' data-show-count='true'>Follow @tgfa_team</a>
				<a href='https://twitter.com/share' class='twitter-share-button' data-url='http://tgfa.net/' data-count='horizontal' data-via='tgfa_team' data-related='tigefa_team:Creator of tgfa.net'>Tweet</a>
				</p>
			</div>
		</div>
	<div id='wrap'>
		<div class='container'></div>			
		
		<div class='container'>
			<div class=\"lang_content\">

			  <div class='row info'>
			   <div class='col-md-4'>
                 <div class='media'>
                   <a class='pull-left' href='//$url/#singleLink'>
                    <i class='fa fa-link fa-4x'></i>
                  </a>
              <div class='media-body'>
               <h4 class='media-heading'>single link</h4>
                anonymize a single link
              </div>
             </div>			   
			   </div>
			   <div class='col-md-4'>
                <div class='media'>
                 <a class='pull-left' href='//$url/#multiLink'>
                   <i class='fa fa-external-link fa-2x'></i>
                    </a>
                   <div class='media-body'>
                   <h4 class='media-heading'>Multi Link</h4>
                    Script to anonymize all the links on your homepage or board
                     </div>
                    </div>			   
    			   </div>
			   <div class='col-md-4'>
               <div class='media'>
               <a class='pull-left' href='//$url/#info'>
                <i class='fa fa-info-circle fa-3x'></i>
                  </a>
                 <div class='media-body'>
                 <h4 class='media-heading'>$sitename</h4>
                Why use $sitename?
                </div>
                </div>			   
			   </div>
			  </div>

			<h2 id=\"singleLink\">anonymize a single link</h2>
			<p>In order to produce a single anonymous link, enter the URL you want to link to and then click on \"Generate URL\".</p>
            <div class='singlelink'>
			<form role='form' name=\"theform\" onSubmit=\"return go();\" action=\"#\">
				<div class='form-group'>
					<label for='link1'>Your URL:</label>
					<input type=\"text\" name=\"nick\" class='form-control input-lg' id='link1' placeholder='http://' /> <br />
					<input class='btn btn-success btn-lg btn-block' type=\"button\" onClick=javascript:go() value=\"Generate URL\" />
			    </div>
					<label for='thelink1'>This is the anonymous URL:</label>
					<textarea name=\"thelink1\" wrap=\"soft\" class='form-control' rows='1'></textarea><br />
					<label for='thelink2'>This is the anonymous URL as an HTML link:</label>
					<textarea class='form-control' rows='1' name=\"thelink2\"></textarea><br />
					<label for='thelink2'>This is the anonymous URL as a board link (works with any board software):</label>

					<textarea class='form-control' rows='1' name=\"thelink3\"></textarea><br />
			</form>
			</div>

			<h2 id=\"multiLink\">Script to anonymize all the links on your homepage or board</h2>
			<p>If you want to anonymize all the external links on your board or homepage, we can generate a
			script for you to deal with this automatically for all your pages. Enter the Sites for which
			links shall not be redirected to anonym.to (e.g. your own) and click on \"Generate script\".</p>
			<br />

			<div class='multilink'>
			<form name=\"displayResult\" onsubmit=\"return false;\" action=\"#\">
				<fieldset class=\"embeddingData\">
					<label for=\"embeddingCode\" accesskey=\"2\" style=\"padding-top: 4px; display: block;\">You only have to place the resulting code at the end of the body area (if possible, directly before the &lt;/body&gt; tag) of your main template. (<a href=\"#anleitung\">detailed instructions</a>)</label>
			
					<textarea class='form-control' rows='4' id=\"embeddingCode\" name=\"embeddingCode\"></textarea><br/>
					<input class=\"btn btn-primary btn-lg btn-block\" type=\"button\" name=\"markAll\" id=\"markAll\" value=\"select all\" onclick=\"document.displayResult.embeddingCode.select();\" />

				</fieldset>
			</form>
			<br />
			<form name=\"anonymizerForm\" onsubmit=\"generateCode('anonymizerForm', 'embeddingCode'); return false;\" action=\"#\">
				<fieldset class=\"generationData\">
					<label for=\"keywordsInput\" accesskey=\"1\" style=\"padding-top: 4px; display: block;\">Do not anonymize the following domains / keywords (comma separated: domain1.tld, domain2.tld, keyword1, ...):</label>
					<input type=\"text\" id=\"keywordsInput\" name=\"keywords\" class=\"form-control input-lg\" /><br />
					<input type=\"submit\" id=\"submitButton\" value=\"generate\" class=\"btn btn-primary btn-lg btn-block\" />			
				</fieldset>
			</form>
			</div>
			<script type=\"text/javascript\">
			   generateCode(\"anonymizerForm\", \"embeddingCode\");
			</script>
			<div class='info'>
			<div class='container'>
			<h3 id=\"info\">The advantages of anonymizing your external links with $sitename</h3>
			<p>Webmasters can use this tool to prevent their site from appearing in the server logs of
			referred pages as referrer. The operators of the referred pages cannot see where their 
			visitors come from any more.<br>
			Using the referrer removal service is quite easy:<br>

			<a target='_blank' href=\"http://$url/?http://www.tigefa.org/\" data-toggle='tooltip' title='first tooltip'>http://$url/?http://www.tigefa.org/</a>
			produces an anonymous link to <a target='_blank' href=\"http://www.tigefa.org/\">tigefa.org</a> which prevents
			the original site from appearing as a referrer in the logfiles of the referred page.</p>
			
			<br /><br />
			
			<h3 id=\"anleitung\">Detailed instructions for the anonymizing script</h3>
			<p>Once the script is embedded in a website, it redirects all the links via $sitename
			- except for the sites that were excluded when generating the script. In vBulletin, for 
			example, you can include the script code in the footer of the global templates
			(Styles&amp;Templates - Global templates - Footer). In Wordpress, use the footer.php.<br>

			Since the script can only anonymize links that have already been loaded at runtime, 
			it's a good idea to place the code as close to the end as possible. Otherwise, links
			that appear after the script code would not be redirected via $sitename.<br />
			<br />
			$sitename only disguises what page a visitor comes from.
            </div>
			</div>

			</div>
		</div>
	</div>	
		<div id=\"footer\">
			<div class='container'>
				<p class='text-muted credit'><i class='tgfa tgfa-blue pull-left'></i>
				Running Version 1.1.0 of <a target='_blank' href='//gitlab.com/tigefa/norefer/'>NoRefer</a>.<br />
				Build with <a target='_blank' href='//getbootstrap.com' data-toggle='tooltip' title='Sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.'>bootsrap theme framework</a><br />
				<a href='https://twitter.com/tgfa_team' class='twitter-follow-button' data-link-color='#2980b9' data-show-count='true'>Follow @tgfa_team</a>
				<a href='https://twitter.com/share' class='twitter-share-button' data-url='http://tgfa.net/' data-count='horizontal' data-via='tgfa_team' data-related='tigefa_team:Creator of tgfa.net'>Tweet</a>
				</p>
			</div>
		</div>
<script src='http://platform.twitter.com/widgets.js'></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44336640-4', 'tgfa.net');
  ga('send', 'pageview');

</script>
<script>
  var _gauges = _gauges || [];
  (function() {
    var t   = document.createElement('script');
    t.type  = 'text/javascript';
    t.async = true;
    t.id    = 'gauges-tracker';
    t.setAttribute('data-site-id', '528fc910f5a1f562ac000074');
    t.src = '//secure.gaug.es/track.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(t, s);
  })();
</script>
</body>
</html>"; }
else echo "
<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
	<meta http-equiv=\"refresh\" content=\"$wait; URL=$s\">
	<title>$sitename - free dereferer service</title>
	<link rel='shortcut icon' href='http://$url/assets/ico/favicon.ico'>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"http://$url/assets/css/bootstrap.min.css\" />
	<style type=\"text/css\">
	html {
		background: #2c3e50;
	}
	body {
		background: #34495e;
		border: 1px solid #666;
		color: #999;
		font: 14px \"Lucida Grande\", \"Lucida Sans Unicode\", tahoma, verdana, arial, sans-serif;
		margin: 5% 10%;
		text-align: center;
	}
	
	a {
		color: #FF8301;
	}
	
	h1 {
		color: #EEE;
	}
	
	#container {
		background: #34495e;
		line-height: 2.4;
		padding: 1em;
	}
	
	p#url {
		font-weight: bold;
		overflow: hidden;
		width: 100%;
	}
.tgfa {
  display: inline-block;
  padding:3px;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.tgfa-white:before {
  content: url('http://$url/assets/img/tgfa-white.png');
}	
	</style>
</head>

<body>

	<h1>$sitename</h1>
	<div id=\"container\">

		
		<p>Please wait while you're being redirected to ...<br />
		<p id=\"url\"><a href=\"$s\">$s</a></p>

<div class='progress progress-bar-success  progress-striped active'>
  <div class='progress-bar' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'>
    <span class='sr-only'>please wait</span>
  </div>
</div>		
		
		<p><span class='tgfa tgfa-white'></span><br /><a href=\"http://$url/\">$sitename</a></p>
		
	</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44336640-4', 'tgfa.net');
  ga('send', 'pageview');

</script>
<script>
  var _gauges = _gauges || [];
  (function() {
    var t   = document.createElement('script');
    t.type  = 'text/javascript';
    t.async = true;
    t.id    = 'gauges-tracker';
    t.setAttribute('data-site-id', '528fc910f5a1f562ac000074');
    t.src = '//secure.gaug.es/track.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(t, s);
  })();
</script> 
</body>
</html>";
?>